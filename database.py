import sqlalchemy as db
from sqlalchemy import MetaData, Table, Column, String, Integer
from sqlalchemy.orm import Session
from sqlalchemy.ext.declarative import declarative_base

class Database():
    db_access = 'postgres+psycopg2://ctc_user:5uper21@192.168.0.19:5432/ctc'
    engine = db.create_engine(db_access)
    def __init__(self):
        self.connection = self.engine.connect()
        print("DB Instance created")

    def fetchByQuery(self, query):
        fetchQuery = self.connection.execute(f"SELECT * FROM {query}")

        for data in fetchQuery.fetchall():
            print(data)

    def fetchUserByName(self):
        meta = MetaData()
        customer = Table('customers', meta, 
                       Column('name'))
        data = self.connection.execute(customer.select())
        for cust in data:
            print(cust)
        """ These are the other columns to be used
                       Column('age'),
                       Column('email'),
                       Column('address'),
                       Column('zip_code'),
        """

    def fetchAllUsers(self):
        # bind an individual Session to the connection
        self.session = Session(bind=self.connection)
        customers = self.session.query(Customer).all()
        for cust in customers:
            print(cust)

    def updateCustomer(self, customerName, address):
        session = Session(bind=self.connection)
        dataToUpdate = {Customer.address: address}
        customerData = session.query(Customer).filter(Customer.name==customerName)
        customerData.update(dataToUpdate)
        session.commit()

    def deleteCustomer(self, customer):
        session = Session(bind=self.connection)
        customerData = session.query(Customer).filter(Customer.name==customer).first()
        session.delete(customerData)
        session.commit()

    def saveData(self, customer):
        session = Session(bind=self.connection)
        session.add(customer)
        session.commit()

        ## self.connection.execute(f"""INSERT INTO customers(name, age, email, address, zip_code) VALUES('{customers.name}', '{customers.age}', '{customers.email}', '{customers.address}', '{customers.zip_code}')""")

Base = declarative_base()

class Customer(Base):
    """Model for customer account."""
    __tablename__ = 'customers'
    name = Column(String)
    age = Column(Integer) 
    email = Column(String)
    address = Column(String)
    zip_code = Column(String)
    id = Column(Integer, primary_key=True)

    def __repr__(self):
        return "<Customer(name='%s', age='%s', email='%s', address='%s', zip_code='%s')>" % (self.name, self.age, self.email, self.address, self.zip_code)


