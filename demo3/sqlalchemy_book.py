import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey
from sqlalchemy import inspect

engine = create_engine('postgres+psycopg2://ctc_user:5uper21@192.168.0.19:5432/ctc')

conn = engine.connect()

metadata = MetaData(conn)

books = Table('book', metadata,
  Column('id', Integer, primary_key=True),
  Column('title', String),
  Column('primary_author',String),
)

metadata.create_all(engine)

inspector = inspect(engine)
print(inspector.get_columns('book'))

