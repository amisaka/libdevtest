import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, Text, MetaData, ForeignKey
from sqlalchemy import insert
from sqlalchemy import inspect

engine = create_engine('postgres+psycopg2://ctc_user:5uper21@192.168.0.19:5432/ctc')

conn = engine.connect()

metadata = MetaData(conn)

customers = Table('customers', metadata,
  Column('name', Text),
  Column('age', Integer),
  Column('email', String),
  Column('address', String),
  Column('zip_code', String),
)

#metadata.create_all(engine)
customers.create()

 
conn.execute(customers.insert(), [ 
    {'name': 'Paul', 
     'age': 23,
     'email': 'paul@gmail.com',
     'address': 'address from paul',
     'zip_code': '2321LL'},
    {'name': 'Felipe', 
     'age': 32,
     'email': 'felipegarcia@gmail.com',
     'address': 'address from felipe',
     'zip_code': '3413MS'},
    {'name': 'Teddy', 
     'age': 90,
     'email': 'teddy@gmail.com',
     'address': 'address from teddy',
     'zip_code': '3423PO'},
    {'name': 'Mark', 
     'age': 17,
     'email': 'mark@gmail.com',
     'address': 'address from mark',
     'zip_code': '9423MA'},
    {'name': 'David', 
     'age': 35,
     'email': 'david@gmail.com',
     'address': 'address from david',
     'zip_code': '2341DA'},
    {'name': 'Allen', 
     'age': 56,
     'email': 'allen@gmail.com',
     'address': 'address from allen',
     'zip_code': '3423PO'},
    {'name': 'James', 
     'age': 56,
     'email': 'james@gmail.com',
     'address': 'address from james',
     'zip_code': '3423PO'},
  ])


