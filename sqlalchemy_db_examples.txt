
import sys
from database import Database, Customer
db = Database()

# List Customer 
db.fetchByQuery("customers")

# Put new customer in a variable
customer = Customer(name="Felipe Test", age=23, email="felipetest@gmail.com", address="felipe test address", zip_code="2323LF")
# Save the new customer to the customer table
db.saveData(customer)

# List all users
db.fetchAllUsers()

# List all users name only
db.fetchUserByName()


db.updateCustomer("Felipe Test", "New Address updating")

db.deleteCustomer("Felipe Test")
